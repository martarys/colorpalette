package pl.eduweb.colorpalette;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ColorAdapter extends RecyclerView.Adapter<ColorViewHolder>{

    private final LayoutInflater layoutInflater;
    private List<String> colors = new ArrayList<>();

    public ColorAdapter(LayoutInflater layoutInflater){

        this.layoutInflater = layoutInflater;
    }


    @NonNull
    @Override
    public ColorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ColorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorViewHolder holder, int position) {
        String colorInHex = colors.get(position);

        holder.setColor(colorInHex);

    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    public void add(String color){
        colors.add(color);
        notifyItemInserted(colors.size()-1);
    }
}

class ColorViewHolder extends RecyclerView.ViewHolder{

    private String color;
    private TextView textView;

    public ColorViewHolder(@NonNull View itemView) {
        super(itemView);
        textView = (TextView) itemView;
    }

    public void setColor(String color) {
        this.color = color;
        textView.setText(color);
        textView.setBackgroundColor(Color.parseColor(color));
    }

    public String getColor() {
        return color;
    }
}
